module LLVMCodeGenerator

import ABC.LLVM
import ABC.Module
import ABC.Parse
import Data.Either
import Data.Error
import Data.Func
import LLVM.AST
import StdFile
import StdList
import StdMisc
import StdOverloaded
import System.File
import Text
import Text.Parsers.Simple.Core

//Start :: *World -> *([ABCInstruction], World)
Start world
	# (mberr, world) = readFileLines "test/fib.abc" world
	| mberr=:(Error _)
		# (Error fe) = mberr
		= abort $ toString fe
	# (Ok ls) = mberr
	# ls = map rtrim ls
	# ls = map parseLine ls
	# mod = case parse parseModule ls of
		(Left err) = abort $ concat err
		(Right mod) = mod
	# (std, world) = stdio world
	= show $ toLLVM mod
