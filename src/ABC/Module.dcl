definition module ABC.Module
/*
 * This grammar is not entirely acurate to the way ABC code is used by the Clean compiler. Part of this is due the fact
 * that the goal of this grammar is to be transpiled to LLVM.
 */
import ABC.Instructions
import Text.Parsers.Simple.Core

/*
 * Encompasses an entire ABC file
 */
:: ABCModule = ABCModule [ABCFunction]

/*
 * Every block whose name does not start with with case. is considered a function
 */
:: ABCFunction = ABCFunction String [ABCBasicBlock]

/*
 * The first block of a function is anonymous, the others are named case.n
 */
:: ABCBasicBlock = ABCBasicBlock (?String) [ABCInstruction]

:: ABCInstrParser a :== Parser ABCInstruction a

parseModule :: ABCInstrParser ABCModule
