definition module ABC.LLVM
/**
 * This module is responsible for converting the given ABC code to LLVM.
 */

import LLVM.AST
from ABC.Module import :: ABCModule

toLLVM :: !ABCModule -> !Module
