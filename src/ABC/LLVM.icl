implementation module ABC.LLVM

import ABC.Module
import Control.Applicative
import Control.Monad => qualified join
import Data.Foldable => qualified forM_, mapM_, sequence_
import Data.Func
import Data.Functor
import Data.Functor.Identity
import LLVM.AST
import LLVM.AST.Global
import LLVM.IR.Builder
import StdArray
import StdBool
import StdFile
import StdFunctions
import StdGeneric
import StdList => qualified sum, all, any, or, and, foldr, foldl
import StdMaybe
import StdOverloaded
import StdString
import StdTuple
import Text => qualified concat

preamble :: [Definition]
preamble = map (GlobalDefinition o Function)
	[ { defaultFunction & returnType = VoidType, name = Name "eqI_b", parameters =
		[Parameter i64 UnNamed, Parameter i64 UnNamed] }
	, { defaultFunction & returnType = i1, name = Name "peek_b_"}
	, { defaultFunction & returnType = VoidType, name = Name "pop_b", parameters =
		[Parameter i64 UnNamed]}
	, { defaultFunction & returnType = VoidType, name = Name "pushI", parameters =
		[Parameter i64 UnNamed]}
	, { defaultFunction & returnType = VoidType, name = Name "push_b", parameters =
		[Parameter i64 UnNamed]}
	, { defaultFunction & returnType = VoidType, name = Name "subI"}
	, { defaultFunction & returnType = VoidType, name = Name "update_b", parameters =
		[Parameter i64 UnNamed, Parameter i64 UnNamed]}
	, { defaultFunction & returnType = VoidType, name = Name "updatepop_b", parameters =
		[Parameter i64 UnNamed, Parameter i64 UnNamed]}
	, { defaultFunction & returnType = VoidType, name = Name "addI"}
	]

toLLVM :: !ABCModule -> Module
toLLVM mod = runBuilder (buildModule mod)

buildModule :: !ABCModule -> IRBuilder Module
buildModule (ABCModule fs) =
	mapM buildFunction fs >>= \defs ->
	pure $ {defaultModule & moduleDefinitions = preamble ++ defs}

buildFunction :: !ABCFunction -> IRBuilder Definition
buildFunction (ABCFunction n bbs) =
	(concat <$> mapM buildBasicBlock bbs) >>= \bbs ->
	pure $ abcFunction n bbs
where
	abcFunction :: !String ![BasicBlock] -> Definition
	abcFunction n bbs = GlobalDefinition $ Function
		{ FunctionData
		| returnType = VoidType
		, name = Name n
		, parameters = []
		, basicBlocks = bbs
		}

/**
 * It is possible for a single ABCBlock to have to be split up into multiple LLVM BasicBlocks,
 * this is due the fact that LLVM does not allow implicit fallthrough of used in Cleans jmp instructions
**/
buildBasicBlock :: !ABCBasicBlock -> IRBuilder [BasicBlock]
buildBasicBlock (ABCBasicBlock mbn is) =
	getName mbn >>= \name ->
	pure (split is) >>= \bbs ->
	freshName >>= \ft ->
	// TODO: Use foldlM
	thd3 <$> foldM foldf (name, ft, []) bbs
where
	getName :: !(?String) -> IRBuilder Name
	getName ?None = freshName
	getName (?Just n) = pure $ Name n

	// Can be optimized
	split :: ![ABCInstruction] -> [[ABCInstruction]]
	split [] = []
	split is
		# (xs, [t:ys]) = span (not o isTerminator) is
		= [xs ++ [t] : split ys]

	foldf :: !(Name, Name, [BasicBlock]) ![ABCInstruction] -> IRBuilder (Name, Name, [BasicBlock])
	foldf (name, ft, bbs) is =
		buildBasicBlock` name ft is >>= \bb ->
		freshName >>= \newft ->
		pure (ft, newft, bbs ++ [bb])

	/**
	 * @param Name of this BB
	 * @param Name of the possible fallthrough
	 * @param The instructions to be converted
	**/
	buildBasicBlock` :: !Name !Name ![ABCInstruction] -> IRBuilder BasicBlock
	buildBasicBlock` name ft is =
		buildInstructions is >>= \(is, ter) ->
		pure $ BasicBlock name is ter
	where
		buildInstructions :: ![ABCInstruction] -> IRBuilder ([Assigned Instruction], Terminator)
		buildInstructions [] = abort "ABC.LLVM: Cannot build BasicBlock from 0 ABCInstructions"
		buildInstructions [i]
			| isTerminator i = buildTerminator i
			| otherwise = abort "ABC.LLVM: ABCBasicBlock ended without a Terminator (jmp, rtn, etc)"
		buildInstructions [i:is]
			| isTerminator i = abort "ABC.LLVM: Found Terminator in ABCBasicBlock, but there are instructions left"
			| otherwise =
				buildInstructions is >>= \(is, t) ->
				buildInstruction i >>= \i ->
				pure ([i:is], t)

		buildInstruction :: !ABCInstruction -> IRBuilder (Assigned Instruction)
		buildInstruction (Ijsr l) = pure o Unassigned $ Call VoidType (Name l) []
		buildInstruction i = pure o Unassigned $ Call VoidType (toFunctionName{|*|} i) (toOperands{|*|} i)

		buildTerminator :: !ABCInstruction -> IRBuilder ([Assigned Instruction], Terminator)
		buildTerminator (Ijmp_true l) =
			freshName >>= \cond ->
			pure (
				[ Assigned cond (Call i1 (Name "peek_b_b") [])
				]
				, CondBr (Local i1 cond) (Name l) ft
			)
		buildTerminator Irtn = pure ([], Ret ?None)
		// TODO: The jmp behavior does not fit well in LLVM
		// This generation should be verified. In particular a jmp in clean is not calling a subroutine.
		// To work around this issue, we place a return directly after the call
		buildTerminator (Ijmp l)
			| startsWith "case." l = pure ([], Br (Name l))
			| otherwise = pure ([Unassigned $ Call VoidType (Name l) []], Ret ?None)

isTerminator :: !ABCInstruction -> Bool
isTerminator (Ijmp _) = True
isTerminator (Ijmp_true _) = True
isTerminator (Ijmp_false _) = True
isTerminator Irtn = True
isTerminator _ = False

generic toOperands a :: !a -> [Operand]
toOperands{|Int|} i = [Constant (Int 64 i)]
toOperands{|IntLiteral|} (IntLit i) = [Constant (Int 64 i)]
toOperands{|IntLiteral|} (LargeIntLit i) = abort "ABC.LLVM: LargeInt unimplemented"
toOperands{|Char|} c = [Constant (Int 8 (toInt c))]
toOperands{|Bool|} b = [Constant (Int 1 (boolToInt b))]
where
	boolToInt :: !Bool -> Int
	boolToInt b = if b 1 0
toOperands{|String|} s = abort "ABC.LLVM: Strings unimplemented"
toOperands{|StringLiteral|} (StringLit s) = abort "ABC.LLVM: String literals unimplemented"
toOperands{|StringWithSpaces|} (StringWithSpaces s) = abort "ABC.LLVM: String literals unimplemented"
toOperands{|(?)|} fx m = abort "ABC.LLVM: Operand of ? unimplemented"
toOperands{|UNIT|} UNIT = []
toOperands{|PAIR|} fx fy (PAIR x y) = fx x ++ fy y
toOperands{|EITHER|} fl _  (LEFT x)  = fl x
toOperands{|EITHER|} _  fr (RIGHT x) = fr x
toOperands{|OBJECT|} fx (OBJECT x) = fx x
toOperands{|CONS of {gcd_name}|} fx (CONS x) = fx x

derive toOperands ABCInstruction, Annotation

generic toFunctionName a :: !a -> Name
toFunctionName{|UNIT|} UNIT = abort "ABC.LLVM: Tried to get function name of UNIT"
toFunctionName{|PAIR|} _ _ _ = abort "ABC.LLVM: Tried to get function name of PAIR"
toFunctionName{|EITHER|} fl _  (LEFT x)  = fl x
toFunctionName{|EITHER|} _  fr (RIGHT x) = fr x
toFunctionName{|OBJECT|} fx (OBJECT x) = fx x
toFunctionName{|CONS of {gcd_name}|} _ _ = Name (gcd_name % (1, size gcd_name - 1))

derive toFunctionName ABCInstruction, Annotation
