implementation module ABC.Module

import ABC.Instructions
import Control.Monad
import Data.Func
import Data.Functor
import StdBool
import Text => qualified join
import Text.Parsers.Simple.Core

parseModule :: ABCInstrParser ABCModule
parseModule = ABCModule <$> pSome parseFunction

parseFunction :: ABCInstrParser ABCFunction
parseFunction =
	pSatisfy (\t -> t=:(Line _)) >>= \(Line name) ->
	// We first parse the anonymous block, after that all blocks must have a name.
	ABCFunction name <$> (parseBasicBlock True >>= \ab -> pMany (parseBasicBlock False) >>= \bs -> pYield [ab:bs])

/**
 * @param Is this an anonymous block
**/
parseBasicBlock :: !Bool -> ABCInstrParser ABCBasicBlock
parseBasicBlock True = ABCBasicBlock ?None <$> pSome parseInstruction
parseBasicBlock False =
	parseName >>= \name ->
	ABCBasicBlock (?Just name) <$> pSome parseInstruction
where
	parseName = pSatisfy verifyName >>= \(Line name) -> pYield name

	verifyName (Line n) = startsWith "case." n
	verifyName _ = False

parseInstruction :: ABCInstrParser ABCInstruction
parseInstruction = pSatisfy (\t -> not t=:(Line _)) >>= \i -> pYield i
