# LLVM-code-generator

Compiles ABC to LLVM IR, this IR can then be compiled together with a compatible runtime system to create an executable.
This is achieved by having the code generator perform next to no operations, it simply takes the ABC instructions and
replaces them with function calls. The runtime system implements all of these functions. When the program is then
compiled with the RTS, the functions are inlined to form the compiled version of the program. This approach also allows
dynamic linking of run-time-systems if the is ever a need.
